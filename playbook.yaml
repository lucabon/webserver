---
- name: "WebserverApache2Deploy"
  hosts: "webserver"

  tasks:
    #######################################################
    # basic server setup
    #

    - name: "remove deprecated/conflicting packages"
      apt:
        state: absent
        name:
        - libapache2-mod-php

    - name: "install webserver dependencies, system utilities, sysadmin tools"
      apt:
        state: present
        name:
        - apache2
        - nullmailer
        - vim
        - screen
        - rsync
        - htop
        - zip
        - unzip
        # fsfe.org - perl/php dependencies
        - libcgi-session-perl
        - libdate-calc-perl
        - libdatetime-perl
        - libdigest-sha-perl
        - libfile-find-rule-perl
        - libgeo-ip-perl
        - libhtml-treebuilder-xpath-perl
        - liblocale-gettext-perl
        - libtemplate-perl
        - libtext-charwidth-perl
        - libtext-format-perl
        - libtext-iconv-perl
        - libtext-wrapi18n-perl
        - libwww-perl
        - libxml-libxml-perl
        - libxml-libxslt-perl
        - libxml-simple-perl
        - libmime-lite-perl # used in merch script
        - perl-base
        - php-sqlite3
        - apache2-suexec-pristine
        - libapache2-mod-fcgid
        - php-fpm
        # reimbursement.php
        - php-mbstring
        # Packages required for certbot
        - certbot
        # rrsync
        - python3-braceexpand

    - name: "install Python2 tools"
      apt:
        state: present
        name:
        - python-certbot-apache
      when: ansible_python.version.major==2

    - name: "install Python3 tools"
      apt:
        state: present
        name:
        - python3-certbot-apache
      when: ansible_python.version.major==3

    - name: Gather installed packages
      package_facts:
        manager: apt
      tags:
        - apache
        - php
        - vhosts

    - name: Set fact for PHP version
      set_fact:
        # Get the installed PHP-FPM version because this defines the location
        # for the override config file
        php_version: "{{ ansible_facts.packages['php-fpm'][0]['version'] | regex_search('[0-9]:([0-9]\\.[0-9])\\+.*', '\\1') | join() }}"
      tags:
        - apache
        - php
        - vhosts

    - name: "make sure required apache2 modules are enabled"
      apache2_module: "name={{item}} state=present"
      with_items:
        - cgid
        - fcgid
        - headers
        - http2
        - proxy
        - proxy_fcgi
        - remoteip
        - rewrite
        - ssl
      tags: apache

    - name: "make sure not required apache2 modules are disabled"
      apache2_module: "name={{item}} state=absent"
      with_items:
        - libapache2-mod-php
      ignore_errors: yes
      tags: apache

    - name: switch off prefork mpm
      apache2_module:
        name: '{{item}}'
        state: absent
        ignore_configcheck: True
      with_items:
        #- cgi
        - php7.3
        - mpm_prefork
      tags: apache

    - name: switch on event mpm
      apache2_module:
        name: mpm_event
        state: present
        ignore_configcheck: True
      notify: apache2_restart
      tags: apache

    - name: "make the webserver default config is disabled"
      file: "path={{item}} state=absent"
      with_items:
        - /etc/apache2/sites-enabled/000-default.conf
        - /etc/apache2/sites-enabled/default-ssl.conf
      notify: apache2_restart
      tags: apache

    - name: "mpm_event config"
      copy:
        dest: /etc/apache2/mods-available/mpm_event.conf
        src: mpm_event.conf
        owner: root
        group: root
        mode: 0644
      notify: apache2_restart
      tags: apache

    - name: "nullmailer : configure mail name"
      copy: "dest='/etc/mailname' content='{{ inventory_hostname }}' owner=root group=root mode=0644"
    - name: "nullmailer : configure smtp server"
      copy: "dest='/etc/nullmailer/remotes' content='{{ smtp_server }}' owner=mail group=mail mode=0600"
    - name: "nullmailer : configure root mail address"
      copy: "dest='/etc/nullmailer/adminaddr' content='' owner=root group=root mode=0644"

    - name: "create www user for holding website data"
      user: name=www uid=1001 home=/srv/www groups=www-data
    - name: "add www-data user to www group"
      user: name=www-data group=www-data groups=www

    # Place rrsync that allows Matomo to download Apache2 logs
    # https://git.fsfe.org/fsfe-system-hackers/matomo
    - name: Ensure the correct rrsync is available
      copy:
        src: rrsync
        dest: /usr/local/bin/rrsync
        owner: root
        group: root
        mode: 0755


    #######################################################
    # letsencrypt certificate
    #

    - name: Create empty list of ACME DNS domains
      set_fact:
        acme_dns_domains: []
      tags: certs

    - name: Append desired domains to ACME DNS domain list
      set_fact:
        # | flatten to have all domains on one level
        acme_dns_domains: "{{ acme_dns_domains + [primary] + aliases | flatten }}"
      loop: "{{ domains | dict2items }}"
      vars:
        # e.g. fsfe.org
        primary: "{{ item.key }}"
        # e.g. ['www.fsfe.org', 'fsfeurope.org']
        aliases: "{{ item.value.aliases | default([]) }}"
        acme_challenge: "{{ item.value.acme_challenge | default('') }}"
      when: acme_challenge == "dns-01"
      tags: certs

    - name: Prepare ACME DNS challenge for specified domains
      include_role:
        name: acme-dns-client
      vars:
        domains: "{{ acme_dns_domains }}"
      tags: certs

    - name: Gather certificates for all domains
      include_role:
        name: certbot-standalone
        apply:
          tags: certs
      vars:
        email: "contact@fsfe.org"
        update_via_apache2: yes
      loop: "{{ domains | dict2items }}"
      tags: always


    #######################################################
    # Apache2 setup
    #

    - name: Create Apache2 maps directory
      file:
        path: /etc/apache2/maps
        state: directory
        owner: root
        group: root
        mode: 0755

    - name: Create Apache2 shared-config directory
      file:
        path: /etc/apache2/shared-config
        state: directory
        owner: root
        group: root
        mode: 0755

    - name: Copy Apache2 maps and special configuration
      copy:
        src: "{{ item.src }}"
        dest: "{{ item.dest }}"
        owner: root
        group: root
        mode: 0644
      loop:
        - { src: "apache2-extra/rewrite_blogs.conf", dest: "/etc/apache2/" }
        - { src: "apache2-extra/rewrite_shortlinks.conf", dest: "/etc/apache2/" }
        - { src: "apache2-extra/maps/blog.txt", dest: "/etc/apache2/maps/" }
        - { src: "apache2-extra/maps/blog_feed.txt", dest: "/etc/apache2/maps/" }
        - { src: "apache2-extra/maps/blog_news.txt", dest: "/etc/apache2/maps/" }
        - { src: "apache2-extra/maps/blog_post_id.txt", dest: "/etc/apache2/maps/" }
        - { src: "apache2-extra/maps/blog_post_title.txt", dest: "/etc/apache2/maps/" }
        - { src: "apache2-extra/shared-config/generic-vhost.conf", dest: "/etc/apache2/shared-config/" }
        - { src: "apache2-extra/shared-config/security.conf", dest: "/etc/apache2/shared-config/" }
        - { src: "apache2-extra/shared-config/ssl.conf", dest: "/etc/apache2/shared-config/" }
      tags: apache

    - name: Create directories for website data
      file:
        path: "{{ item }}"
        state: directory
        mode: 0755
        owner: www
        group: www
      loop:
        - /srv/www/fsfe.org
        - /srv/www/test.fsfe.org

    - name: Create symbolic link for fsfe.org
      file:
        src: /srv/www/fsfe.org
        dest: /srv/www/html
        state: link
        owner: www
        group: www

    - name: Configure apache2 logging for ip-proxy
      lineinfile:
        path: /etc/apache2/apache2.conf
        regexp: '^LogFormat.+ combined_proxy$'
        insertafter: '^LogFormat.+ combined$'
        line: 'LogFormat "%a %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined_proxy'
      when:
        - ip_proxy is defined
        - ip_proxy
      notify: apache2_restart
      tags: apache

    - name: Configure Apache2 sites
      include_role:
        name: apache2-site
        apply:
          tags:
            - vhosts
            - apache
      loop: "{{ domains | dict2items }}"
      tags: always


    #######################################################
    # PHP Configuration
    #

    - name: Copy additional custom configuration (e.g. PHP)
      copy:
        src: "{{ item.src }}"
        dest: "{{ item.dest }}"
        owner: root
        group: root
        mode: 0644
      loop:
        - { src: "php/99-custom.ini", dest: "/etc/php/{{ php_version}}/fpm/conf.d/99-custom.ini" }
      notify: php_fpm_restart
      tags:
        - apache
        - php


    #######################################################
    # Logging options
    #

    - name: Modify logrotate options for Apache2
      lineinfile:
        path: /etc/logrotate.d/apache2
        regexp: "{{ item }}"
        line: '\1# \2'
        backrefs: yes
      loop:
        - '^(\s*)(delaycompress)$'
        - '^(\s*)(notifempty)$'

  handlers:
    - name: apache2_reload
      service: name=apache2 state=reloaded
    - name: apache2_restart
      service: name=apache2 state=restarted
    - name: php_fpm_restart
      service: name=php{{ php_version }}-fpm state=restarted
